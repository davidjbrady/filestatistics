# Updated on 15 August 2015 and put into GIT repository

''' A file with a name like picture.jpg is said to have an extension of "jpg";
    i.e. the extension of a file is the part of the file after the final
    period in its name. Write a program that takes as an argument the name of
    a directory (folder) and then finds the extension of each file. Then, for
    each extension found, it prints the number of files with that extension and
    the minimum, average, and maximum size for files with that extension in the
    selected directory.'''
# Test commit
import os
import sys
import traceback
from optparse import OptionParser


def getFileExtDict(flist):
    ''' The purpose of getFileExtDict is to build a dictionary keyed off file extensions within the list. '''
    feDict = {}
    for f in flist:
        try:
            # A KeyError will be thrown if the key does not exist
            feDict[f[2]].append(f)
        except(KeyError):
            # Initialize an empty list to ensure we get a list of lists
            feDict[f[2]] = []
            feDict[f[2]].append(f)
    
    return feDict            
    
def getFileExtStats(filelist):
    '''Function getFileExtStats accepts a list that contains files that all share the same file extension.
       Statistics calculated include the number of files, the minimum and maximum file sizes, and the average file size.'''
    count, maximum, minimum, average, total = 0, 0, 0, 0, 0
    minFileName, maxFileName = "", ""
    for element in filelist:
        # We don't want to count a sub-directory if it made it into the list
        if not os.path.isdir(element[3] + "\\" + element[0]):
            if count == 0:
                minFileName = element[0]
                minimum = element[1]
            if element[1] < minimum:
                minFileName = element[0]
                minimum = element[1]
            if element[1] > maximum:
                maxFileName = element[0]
                maximum = element[1]
            total += element[1]
            count += 1
            count = len(filelist)
            average = total/count
    return [count, [minFileName, minimum], [maxFileName, maximum], average]

def printFileStats(statlist, x, fpath, doIt):
    ''' This function is a convenience function used to display file statistics determined by getFileExtStats. '''
    if statlist[0] > 0:
        if doIt == True:
            sys.stdout.write("There are {0} files in {1} and its sub-directories with a file extension of \"{2}\"\n".format(statlist[0], fpath, x))
        else:
            sys.stdout.write("There are {0} files in {1} with a file extension of \"{2}\"\n".format(statlist[0], fpath, x))
        sys.stdout.write("Minimum file name is {0} and the size is size is {1}\n".format(statlist[1][0],statlist[1][1]))
        sys.stdout.write("Maximum file name is {0} and the size is size is {1}\n".format(statlist[2][0],statlist[2][1]))
        sys.stdout.write("Average file size is {0:.2f}\n\n".format(statlist[3]))

def printHeader():
    ''' The printHeader function prints a header if the user chooses to "walk" the given directory's sub-directories. '''
    print("\n")
    print("*******************************************************")
    print("*                                                     *")
    print("*         SUMMARY FILE EXTENSION STATISTICS           *")
    print("*                                                     *")
    print("*******************************************************")
    print("\n")


def main():

    parser = OptionParser()
    (options, args) = parser.parse_args()
    if len(args) > 0 and os.path.isdir(args[0]): # We can assume a path was passed into the program
        walkTree = False
        verbose = False
        fpath = args[0]
        interactive = False
        print("Path is " + fpath)
    else:
        interactive = True
        docResponse = input("Do you wish to see the doc string for this program (enter Y or N): ")
        
        if docResponse[0].upper() == 'Y':
            sys.stdout.write(__doc__ + "\n\n")
        else:
            sys.stdout.write("\n")

        response = input("Do you want to calculate file statistics for the specified directory only?(enter Y or N): ")
        #response = sys.stdin.readline()
        if response[0].upper() == 'Y':
            walkTree = False
        elif response[0].upper() == 'N':
            walkTree = True
            response = input("\nDo you want to use \"verbose mode\" to calcuate statistics for every sub-directory(enter Y or N): ")
            if response[0].upper() == 'Y':
                verbose = True
            else:
                verbose = False
        else: # Something happened so default to only look at the directory specified by the user.
            walkTree = False
            verbose = False

    doIt = True # Keep repeating the program until file interrupt <Ctrl-C> terminates the loop.

    # The list "fullList" is only used if the os.walk function is used to recurse through a
    # directory and its sub-directories. This list is used to calculate summary statistics.
    fullList = []
 
    try:
        sys.stdout.write("\nUse the file interrupt <Ctrl-C> to terminate the program.\n\n")
        while doIt == True:
            if len(fullList) > 0: # Will be an empty list unless walkTree = True and one pass has been made.
                printHeader()
                fileSumExtDict = getFileExtDict(fullList)
                sortSumKeyList = list(fileSumExtDict.keys())
                sortSumKeyList.sort()
                for key in sortSumKeyList:
                    sumExtList = fileSumExtDict[key]
                    x = sumExtList[0][2]
                    statlist = getFileExtStats(sumExtList)
                    printFileStats(statlist, x, fpath, doIt)
                    
            fullList = [] # Reset fullList to an empty list before the next path is entered by the user
            if interactive == True:
                fpath = input("Enter a path: ")
                sys.stdout.write("\n")
            if os.path.isdir(fpath):
                if walkTree == True:
                    for root, subDir, files in os.walk(fpath):
                        try:
                            filelist = list(map(lambda f: [f, os.path.getsize(os.path.join(root, f)), os.path.splitext(f)[1][1:], root], files))
                            filterlist = list(filter(lambda x: os.path.isfile(os.path.join(root, x[0])) and x and not os.path.isdir(os.path.join(root, x[0])), filelist))
                        except PermissionError as pe:
                            sys.stdout.write("\nProgram caught a Permission Error: {0}\n\n".format(pe))
                            continue
                        except:
                            traceback.print_exc(file=sys.stdout)
                            continue
                        # Build a list of files as os.walk recursively "walks" the selected directory and its sub-directories.
                        for element in filterlist:
                            fullList.append(element)

                        # Calculate file statistics for the selected directory and each sub-directory only if "verbose" is True.
                        if verbose == True:
                            # Create a dicionary keyed by the file extension
                            fileExtDict = getFileExtDict(filterlist)
                            sortKeyList = list(fileExtDict.keys())
                            sortKeyList.sort()
                            for key in sortKeyList:
                                try:
                                    extlist = fileExtDict[key]
                                except(KeyError):
                                    continue
                                statlist = getFileExtStats(extlist)
                                if statlist[0] > 0:
                                    printFileStats(statlist, root, extlist[0][2], doIt)
                else: # Only consider the directory specified by the user.
                    if interactive == False: # Then we only want to execute the program once.
                        doIt = False
                    files = os.listdir(fpath)
                    try:
                        # Build a list of all files in the directory
                        filelist = list(map(lambda f: [f, os.path.getsize(os.path.join(fpath, f)), os.path.splitext(f)[1][1:], fpath], files))
                    except PermissionError as pe:
                        sys.stdout.write("\nProgram caught a Permission Error: {0}\n\n".format(pe))
                        continue
                    except:
                        traceback.print_exc(file=sys.stdout)
                        continue
                    # Create a dicionary keyed by the file extension
                    fileExtDict = getFileExtDict(filelist)

                    sortKeyList = list(fileExtDict.keys())
                    sortKeyList.sort()
                    for key in sortKeyList:
                        extlist = fileExtDict[key]
                        statlist = getFileExtStats(extlist)
                        printFileStats(statlist, extlist[0][2], fpath, doIt)

            else:
                sys.stdout.write("Please enter a valid path\n")
    except KeyboardInterrupt:
        doIt = False
        print("\n")
        print("Interrupt key was pressed. Exiting loop.\n")
    except:
        tb = traceback.format_exc()
        print(tb + "\n")

            
if __name__ == "__main__":
    main()
